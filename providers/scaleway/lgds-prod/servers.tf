# Create a security group
resource "scaleway_instance_security_group" "web" {
  inbound_default_policy  = "drop"
  outbound_default_policy = "accept"

  # Allow any IP to SSH
  # Mostly because we launch application deployments from Gitlab.com
  # and Gitlab.com doesn't offer stable outgoing IPs for gitlab-runners.
  inbound_rule {
    action = "accept"
    port   = "22"
  }

  inbound_rule {
    action = "accept"
    port   = "80"
  }

  inbound_rule {
    action = "accept"
    port   = "443"
  }
}

# Create a public IP
resource "scaleway_instance_ip" "public_ip" {
  count = 0
}

resource "scaleway_instance_server" "membres-prod" {
  count = 0
  type  = "DEV1-M"
  image = "ubuntu-bionic"
  tags  = ["lgds", "membres", "prod", "ubuntu"]

  ip_id             = scaleway_instance_ip.public_ip[count.index].id
  security_group_id = scaleway_instance_security_group.web.id

  lifecycle {
    ignore_changes = [image, tags]
  }
}
