locals {
  region = "fr-par"
}

# Authentication is done via either scaleway classical env variables
# or via pass password manager:
# - Store your scaleway access key in 'pass terraform/scaleway/lgds-prod/access_key' path
# - Store your scaleway secret key in 'pass terraform/scaleway/lgds-prod/secret' path
provider "scaleway" {
  zone    = "${local.region}-1"
  region  = local.region
  version = "~> v1.15.0"
}
