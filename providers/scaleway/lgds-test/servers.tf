locals {
  # List of trusted IPs (For sys admins)
  trusted_ips = [
    "86.238.179.43", # Paul
    "34.77.110.38"   # lgds.druidoo.io (Server access for backups)
  ]
}

# Create a security group
resource "scaleway_instance_security_group" "web" {
  inbound_default_policy  = "drop"
  outbound_default_policy = "accept"


  dynamic "inbound_rule" {
    for_each = local.trusted_ips

    content {
      action = "accept"
      port   = 22
      ip     = inbound_rule.value
    }
  }

  # Allow any IP to SSH (mostly because Gitlab.com doesn't offer stable outgoing IPs for gitlab-runners)
  inbound_rule {
    action = "accept"
    port   = "22"
  }

  inbound_rule {
    action = "accept"
    port   = "80"
  }

  inbound_rule {
    action = "accept"
    port   = "443"
  }
}

# Create a public IP
resource "scaleway_instance_ip" "public_ip" {
  count = 0
}

resource "scaleway_instance_server" "odoo-test" {
  count = 0
  type  = "DEV1-S"
  image = "ubuntu-bionic"
  tags  = ["lgds", "test", "ubuntu"]

  ip_id             = scaleway_instance_ip.public_ip[count.index].id
  security_group_id = scaleway_instance_security_group.web.id

  lifecycle {
    ignore_changes = [image, tags]
  }
}
